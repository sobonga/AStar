/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package astar;

/**
 *
 * @author sobongad
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class AStar {

    public static final int DIAGONAL_COST = 2;
    public static final int V_H_COST = 2;

    static class Cell {

        int heuristicCost = 0; //Heuristic cost
        int finalCost = 0; //G+H
        int i, j;
        Cell parent;

        Cell(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public String toString() {
            return "[" + this.i + ", " + this.j + "]";
        }
    }

    static Cell[][] grid = new Cell[5][5];

    static PriorityQueue<Cell> open;

    static boolean closed[][];
    static int startI, startJ;
    static int endI, endJ;

    public static void setBlocked(int i, int j) {
        grid[i][j] = null;
    }

    public static void setStartCell(int i, int j) {
        startI = i;
        startJ = j;
    }

    public static void setEndCell(int i, int j) {
        endI = i;
        endJ = j;
    }

    static void checkAndUpdateCost(Cell current, Cell t, int cost) {
        if (t == null || closed[t.i][t.j]) {
            return;
        }
        int t_final_cost = t.heuristicCost + cost;

        boolean inOpen = open.contains(t);
        if (!inOpen || t_final_cost < t.finalCost) {
            t.finalCost = t_final_cost;
            t.parent = current;
            if (!inOpen) {
                open.add(t);
            }
        }
    }

    public static void AStar() {

        open.add(grid[startI][startJ]);

        Cell current;

       while(true){ 
            current = open.poll();
            if(current==null)break;
            closed[current.i][current.j]=true; 

            if(current.equals(grid[endI][endJ])){
                return; 
            } 

            Cell t;  
            if(current.i-1>=0){
                t = grid[current.i-1][current.j];
                checkAndUpdateCost(current, t, current.finalCost+V_H_COST); 

                if(current.j-1>=0){                      
                    t = grid[current.i-1][current.j-1];
                    checkAndUpdateCost(current, t, current.finalCost+DIAGONAL_COST); 
                }

                if(current.j+1<grid[0].length){
                    t = grid[current.i-1][current.j+1];
                    checkAndUpdateCost(current, t, current.finalCost+DIAGONAL_COST); 
                }
            } 

            if(current.j-1>=0){
                t = grid[current.i][current.j-1];
                checkAndUpdateCost(current, t, current.finalCost+V_H_COST); 
            }

            if(current.j+1<grid[0].length){
                t = grid[current.i][current.j+1];
                checkAndUpdateCost(current, t, current.finalCost+V_H_COST); 
            }

            if(current.i+1<grid.length){
                t = grid[current.i+1][current.j];
                checkAndUpdateCost(current, t, current.finalCost+V_H_COST); 

                if(current.j-1>=0){
                    t = grid[current.i+1][current.j-1];
                    checkAndUpdateCost(current, t, current.finalCost+DIAGONAL_COST); 
                }
                
                if(current.j+1<grid[0].length){
                   t = grid[current.i+1][current.j+1];
                    checkAndUpdateCost(current, t, current.finalCost+DIAGONAL_COST); 
                }  
            }
        } 
    
    }


    public static void test(int tCase, int x, int y, int si, int sj, int ei, int ej, int[][] blocked) {
   //  ei, ej = end location's x and y coordinates
   //  int[][] blocked = array containing inaccessible cell coordinates(~)
   //x, y = Grid's dimensions
   //  si, sj = start location's x and y coordinates
        System.out.println("\n\nTest Case #" + tCase);
        // tCase = test case No.
        //Reset
        grid = new Cell[x][y];
        closed = new boolean[x][y];
        open = new PriorityQueue<>((Object o1, Object o2) -> {
            Cell c1 = (Cell) o1;
            Cell c2 = (Cell) o2;

            return c1.finalCost < c2.finalCost ? -1
                    : c1.finalCost > c2.finalCost ? 1 : 0;
        });
        //Set start position(@)
        setStartCell(si, sj);  //Setting to 0,0 by default. Will be useful for the UI part

        //Set End Location(X)
        setEndCell(ei, ej);

        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < y; ++j) {
                grid[i][j] = new Cell(i, j);
                grid[i][j].heuristicCost = Math.abs(i - endI) + Math.abs(j - endJ);
//                  System.out.print(grid[i][j].heuristicCost+" ");
            }
//              System.out.println();
        }
        grid[si][sj].finalCost = 0;

        for (int i = 0; i < blocked.length; ++i) {
            setBlocked(blocked[i][0], blocked[i][1]);
        }

        //To isplay initial map with walkable choices
        System.out.println("Grid: ");
        String gridContentLine = null;
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < y; ++j) {
                if (i == si && j == sj) {
                    gridContentLine = "@  ";
                    System.out.print("@  "); //Start 
                } else if (i == ei && j == ej) {
                    gridContentLine +="X  ";
                    System.out.print("X  ");  //Destination
                } else if (grid[i][j] != null) {
                    gridContentLine += "# ";
                    System.out.printf("# ", 0); // '#' Defines walkable tiles in the grid
                } else {
                    gridContentLine += "~  ";
                    System.out.print("~  "); // '~' Non walkable tiles
                }
                contentWriter(gridContentLine);
            }
            
           
            System.out.println();
        }
        
        System.out.println();
        System.out.println("Possible path marked with #(Walkable tiles)");
        System.out.println("Non walkable tiles denoted by ~");

        AStar();
        System.out.println("\nScores for cells: ");
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < x; ++j) {
                if (grid[i][j] != null) {
                    System.out.printf("%-3d ", grid[i][j].finalCost);
                } else {
                    System.out.print("~  ");
                }
            }
            System.out.println();
        }
        System.out.println();

        if (closed[endI][endJ]) {
            //Trace back the path 
            System.out.println("Path: ");
            Cell current = grid[endI][endJ];
            System.out.print(current);
            while (current.parent != null) {
                System.out.print(" -> " + current.parent);
                current = current.parent;
            }
            System.out.println();
            System.out.println("Trace back the path");
        } else {
            System.out.println("No possible path"); 
        }
    }

    public static int getValue(char c){
   int i=0;
        switch(c){
            case '*':
                i=2;
                break;
            case '~':
                i=0;
                break;
            case '.':
                case '@':
                  case 'X':  
            i=1;
                break;
            case '^':
                i=3;
                break;
                
                
    }
        return i;
    }
    
    public static int getSIzeOfTxt(String fileName){
        BufferedReader br = null;
        int count = 0;
        try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));
                        
                        
                        
			while ((sCurrentLine = br.readLine()) != null) {
                                count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
        
        return count;
    }
    public static void main(String[] args) throws Exception {
            String fileName = "Large Map\\large_map.txt";
                 String [] sts = new String[getSIzeOfTxt(fileName)];
                 /*
                 code read file
                 
                 */
       BufferedReader br = null;

		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));
                        
                        
			for(int x  = 0; x < sts.length; x++) {				
                                sts[x] = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
        
        ArrayList<int[]> lst = new ArrayList(); 
        int forx=0;
        int [] start=null;
        int[] end =null;
        
                 for(int i=0;i<sts.length;i++){
                 char ch[] =sts[i].toCharArray();
                 if(forx<ch.length){
                 forx=ch.length;
                 }
                     for(int j=0;j<ch.length;j++){
                         if(getValue(ch[j])>0){
                         map.put(new int[]{i,j},getValue(ch[j]) );
                        
                         }
                         if(ch[j]=='~'){
                         lst.add(new int []{i,j});
                         }
                         if(ch[j]=='X'){
                         end=new int[]{i,j};
                         }
                         if(ch[j]=='@'){
                             start=new int[]{i,j};
                         }
                 }
                 
                 }
        int[][] temp=new int[lst.size()][];
        lst.toArray(temp);
        test(1, forx, sts.length, start[0], start[1],end[0] , end[1],temp);
    }
    
    public static void contentWriter(String content) {
		try {

			File file = new File("Large Map/output.txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

                        
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
                        bw.newLine();
			bw.close();


		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
    static Map<int[],Integer> map = new HashMap<int[], Integer>();
}